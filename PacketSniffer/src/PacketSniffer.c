/*
 ============================================================================
 Name        : PacketSniffer.c
 Author      : André Miguel Tavares Martins
 Version     :
 Description : Packet sniffer
 ============================================================================
 */

#include "PacketSniffer.h"

int main(int argc, char *argv[]) {

	char *dev=malloc(sizeof(char)*20);
	pthread_rwlock_init(&blockPacket, NULL);

	printf("********************************************* Warning ****************************************\n");
	printf("| If it is not the first time using the program please delete the created files named tid[x] |\n");
	printf("**********************************************************************************************\n");

	//Validation process

	if(argc!=2){
		printf("############### ERROR #### Invalid number of arguments! ################\n");
		printf("#               Try to use './PacketSniffer <interface>'               #\n");
		printf("########################################################################\n");
		exit(1);
	}else{
		if(validateIface(argv[1]) == 0){
			printf("################ ERROR #### Invalid interface name ################\n");
			printf("#      Try to introduce a valid name (It is case-sensitive)       #\n");
			printf("###################################################################\n");
			exit(2);
		}else{
			printf("\nValid interface inserted -> %s\n",argv[1]);
		}
	}

	strcpy(dev,argv[1]);

	// Sniffing process
	// Consulted on: http://www.tcpdump.org/pcap.html
	// Consulted on: http://www.binarytides.com/packet-sniffer-code-c-libpcap-linux-sockets/

	pcap_t *handle;
	char errbuf[PCAP_ERRBUF_SIZE];
	struct bpf_program fp; // Compiled filter

	char filter_exp[] = "tcp"; // Filter expression

	// Open the session in promiscuous (1) non-promiscuous (0) mode

	handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(3);
	}

	// Compile and apply the filter
	if (pcap_compile(handle, &fp, filter_exp, 0, PCAP_NETMASK_UNKNOWN) == -1) {
		fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
		exit(4);
	}

	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
		exit(5);
	}

	pcap_loop(handle, NUMBER_OF_PACKETS, process_packet, NULL);
	//if second argument is negative sniffs until an error occurs

	//Queue handle

	int i;

	for(i=0;i<NUMBER_OF_PACKETS;i++){

		printf("\nChecking streams!\n");

		//mutex lock
		pthread_rwlock_wrlock(&blockPacket);
		memcpy(packetToPrint,(const char*)removeData(),BUFSIZ);
		packetsOut++;
		printf("\n***** PacketsOut = %d *****\n",packetsOut);
		//mutex unlock
		streamExists(packetToPrint);
		pthread_rwlock_unlock(&blockPacket);
		sleep(2);	//Time between packets to print in the files;
	}

	// *****************************************************************************************************
	// Clean threads, wait for join and clean mutex

	pthread_rwlock_wrlock(&blockPacket);
	packetsOut++;	//to break the loop of the threads;
	pthread_rwlock_unlock(&blockPacket);

	for(i=0;i<=StreamNumber;i++){
		sleep(1);
    	pthread_mutex_lock(&mut[i]);
    	pthread_cond_signal(&condition[i]);
    	pthread_mutex_unlock(&mut[i]);
	}

	for(i=0;i<=StreamNumber;i++)
		pthread_join(tid[i],NULL);

	for(i=0;i<NUMBER_OF_PACKETS;i++)
		pthread_mutex_destroy(&mut[i]);

	pthread_rwlock_destroy(&blockPacket);

	// Close the session
	pcap_close(handle);

	free(dev);
	return EXIT_SUCCESS;
}

int validateIface(char *ifaceName){

	struct ifaddrs *ifaddr, *ifa;
	int valid = 0;

	// Consulted on the manpage: http://man7.org/linux/man-pages/man3/getifaddrs.3.html

	if (getifaddrs(&ifaddr) == -1) {
		printf("ERROR getting the interfaces information\n");
	    exit(EXIT_FAILURE);
	}

	for(ifa=ifaddr;ifa!=NULL;ifa=ifa->ifa_next)
	{
		if (ifa->ifa_addr == NULL)
			continue;

	    //if (ifa->ifa_addr && ifa->ifa_addr->sa_family == AF_PACKET)
	        //printf("%s\n", ifa->ifa_name);

	    if(strcmp(ifaceName,ifa->ifa_name)==0)
	    	valid = 1;
	}

	freeifaddrs(ifaddr);
	return valid;
}

void process_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *buffer)
{
	//Queue functions and implementation consulted on: https://www.tutorialspoint.com/data_structures_algorithms/queue_program_in_c.htm

	//Store the captured packets in a queue with length = NUMBER_OF_PACKETS;

    size = header->len;
    insert(buffer);

    //Print the received tcp packet

    print_tcp_packet(buffer,size);
}

/*
 * Queue functions modified from https://www.tutorialspoint.com/data_structures_algorithms/queue_program_in_c.htm
 */

void insert(const u_char *buffer) {

	if(!isFull()) {

		if(rear == NUMBER_OF_PACKETS-1) {
			rear = -1;
		}

		memcpy(packetArray[++rear],(const char*)buffer,BUFSIZ);
		//print_tcp_packet(packetArray[aux] , size);

		itemCount++;
		printf("\nPacket inserted!\n");
   }
}

bool isFull() {
   return itemCount == NUMBER_OF_PACKETS;
}

bool isEmpty() {
   return itemCount == 0;
}

int sizeQueue() {
   return itemCount;
}

u_char * removeData() {

	u_char data[BUFSIZ];

	memcpy(data,packetArray[front++],BUFSIZ);

   if(front == NUMBER_OF_PACKETS) {
      front = 0;
   }

   itemCount--;
   return data;
}

// **************** End queue functions ***************************************************************************

int streamExists(const u_char *buffer){

	int foundSrc = -1;
	int foundDst = -1;

    unsigned short iphdrlen;

    struct iphdr *iph = (struct iphdr *)( buffer  + sizeof(struct ethhdr) );
    iphdrlen = iph->ihl*4;

    struct tcphdr *tcph=(struct tcphdr*)(buffer + iphdrlen + sizeof(struct ethhdr));

    u_int16_t auxPortSrc = ntohs(tcph->source);
    u_int16_t auxPortDst = ntohs(tcph->dest);

    struct sockaddr_in auxIPsrc;
    struct sockaddr_in auxIPdst;

    memset(&auxIPsrc, 0, sizeof(auxIPsrc));
    memset(&auxIPdst, 0, sizeof(auxIPdst));

    auxIPsrc.sin_addr.s_addr = iph->saddr;
    auxIPdst.sin_addr.s_addr = iph->daddr;

    int c;
    for(c=0;c<NUMBER_OF_PACKETS;c++){
    	if((auxIPsrc.sin_addr.s_addr == streamsStored[c].ipAndPortSrc.ipPack.sin_addr.s_addr) && (auxPortSrc == streamsStored[c].ipAndPortSrc.port)){
    		foundSrc = c;
    	}
    	if((auxIPsrc.sin_addr.s_addr == streamsStored[c].ipAndPortDst.ipPack.sin_addr.s_addr) && (auxPortSrc == streamsStored[c].ipAndPortDst.port)){
    		foundSrc = c;
    	}
    	if((auxIPdst.sin_addr.s_addr == streamsStored[c].ipAndPortSrc.ipPack.sin_addr.s_addr) && (auxPortDst == streamsStored[c].ipAndPortSrc.port)){
    		foundDst = c;
    	}
    	if((auxIPdst.sin_addr.s_addr == streamsStored[c].ipAndPortDst.ipPack.sin_addr.s_addr) && (auxPortDst == streamsStored[c].ipAndPortDst.port)){
    		foundDst = c;
    	}
    }

    //Debug print ***************************
    //printf("\n** foundSRC = %d **",foundSrc);
    //printf("\n** foundDst = %d **",foundDst);
    // **************************************

    if((foundSrc == -1) || (foundDst==-1) || (foundSrc != foundDst)){
    	//printf("\n\n\nNew stream in position %d\n\n\n",StreamNumber);

    	//Start new thread to the stream
    	int rc;


    	rc = pthread_create(&tid[StreamNumber], NULL, PrintFile, (void *)StreamNumber);
    	if (rc){
    		printf("ERROR; return code from pthread_create() is %d\n", rc);
    		exit(-1);
    	}
    	printf("\nNew thread created -> tid[%d]\n",StreamNumber);

    	memset(&streamsStored[StreamNumber].ipAndPortSrc.ipPack, 0, sizeof(streamsStored[StreamNumber].ipAndPortSrc.ipPack));
    	streamsStored[StreamNumber].ipAndPortSrc.ipPack.sin_addr.s_addr = iph->saddr;
    	streamsStored[StreamNumber].ipAndPortSrc.port = ntohs(tcph->source);

    	memset(&streamsStored[StreamNumber].ipAndPortDst.ipPack, 0, sizeof(streamsStored[StreamNumber].ipAndPortDst.ipPack));
    	streamsStored[StreamNumber].ipAndPortDst.ipPack.sin_addr.s_addr = iph->daddr;
    	streamsStored[StreamNumber].ipAndPortDst.port = ntohs(tcph->dest);

    	StreamNumber ++;
    }else if((foundSrc == foundDst) && (foundSrc+foundDst)>=0){
    	printf("\nStream on position: %d\n",foundSrc);

    	pthread_mutex_lock(&mut[foundSrc]);
    	pthread_cond_signal(&condition[foundSrc]);
    	pthread_mutex_unlock(&mut[foundSrc]);
    }

    return c;
}

void *PrintFile(void *streamN){

	int streamNumber = (int)streamN;

	while(1){

		char filename[10];

		sprintf(filename,"tid[%d]",streamNumber);
		FILE *fp;
		fp = fopen(filename, "a");

		unsigned short iphdrlen;

		struct iphdr *iph = (struct iphdr *)( packetToPrint  + sizeof(struct ethhdr) );
		iphdrlen = iph->ihl*4;

		struct tcphdr *tcph=(struct tcphdr*)(packetToPrint + iphdrlen + sizeof(struct ethhdr));

		fprintf(fp,"\n\n***********************TCP Packet*************************\n");
		fprintf(fp,"\n");
		fprintf(fp,"Packet number [%d] of the queue\n",packetsOut);
		fprintf(fp,"TCP Header\n");
		fprintf(fp,"   |-Source Port      : %u\n",ntohs(tcph->source));
		fprintf(fp,"   |-Destination Port : %u\n",ntohs(tcph->dest));

		memset(&source, 0, sizeof(source));
		source.sin_addr.s_addr = iph->saddr;

		memset(&dest, 0, sizeof(dest));
		dest.sin_addr.s_addr = iph->daddr;

		fprintf(fp,"\n");
		fprintf(fp,"IP Header\n");
		fprintf(fp,"   |-Source IP        : %s\n" , inet_ntoa(source.sin_addr) );
		fprintf(fp,"   |-Destination IP   : %s\n" , inet_ntoa(dest.sin_addr) );
		fprintf(fp,"\n###########################################################");

		fclose(fp);

		pthread_mutex_lock(&mut[streamNumber]);
		pthread_cond_wait(&condition[streamNumber], &mut[streamNumber]);
		pthread_mutex_unlock(&mut[streamNumber]);

		if(packetsOut>NUMBER_OF_PACKETS)
			break;
	}
	printf("Cicle break on thread tid[%d]\n",streamNumber);
	return EXIT_SUCCESS;
}
// Auxiliary code to see the live captured packets --------------------------------------------------------------------------------

void print_ethernet_header(const u_char *Buffer, int Size)
{
    struct ethhdr *eth = (struct ethhdr *)Buffer;

    printf("\n");
    printf("Ethernet Header\n");
    printf("   |-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_dest[0] , eth->h_dest[1] , eth->h_dest[2] , eth->h_dest[3] , eth->h_dest[4] , eth->h_dest[5] );
    printf("   |-Source Address      : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_source[0] , eth->h_source[1] , eth->h_source[2] , eth->h_source[3] , eth->h_source[4] , eth->h_source[5] );
    printf("   |-Protocol            : %u \n",(unsigned short)eth->h_proto);
}

void print_ip_header(const u_char * Buffer, int Size)
{
    print_ethernet_header(Buffer , Size);

    unsigned short iphdrlen;

    struct iphdr *iph = (struct iphdr *)(Buffer  + sizeof(struct ethhdr) );
    iphdrlen =iph->ihl*4;

    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = iph->saddr;

    memset(&dest, 0, sizeof(dest));
    dest.sin_addr.s_addr = iph->daddr;

    printf("\n");
    printf("IP Header\n");
    printf("   |-IP Version        : %d\n",(unsigned int)iph->version);
    printf("   |-Source IP        : %s\n" , inet_ntoa(source.sin_addr) );
    printf("   |-Destination IP   : %s\n" , inet_ntoa(dest.sin_addr) );
}

void print_tcp_packet(const u_char * Buffer, int Size)
{
    unsigned short iphdrlen;

    struct iphdr *iph = (struct iphdr *)( Buffer  + sizeof(struct ethhdr) );
    iphdrlen = iph->ihl*4;

    struct tcphdr *tcph=(struct tcphdr*)(Buffer + iphdrlen + sizeof(struct ethhdr));

    printf("\n\n***********************TCP Packet*************************\n");

    print_ip_header(Buffer,Size);

    printf("\n");
    printf("TCP Header\n");
    printf("   |-Source Port      : %u\n",ntohs(tcph->source));
    printf("   |-Destination Port : %u\n",ntohs(tcph->dest));
    printf("   |-Sequence Number    : %u\n",ntohl(tcph->seq));
    printf("\n###########################################################");
}
