/*
 * PacketSniffer.h
 *
 *  Created on: Nov 16, 2016
 *      Author: André Miguel Martins
 */

#ifndef PACKETSNIFFER_H_
#define PACKETSNIFFER_H_

#include <stdio.h>
#include <stdlib.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <pcap.h>
#include <netinet/ether.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <pthread.h>
#include <stdbool.h>

#include<arpa/inet.h>
#include<netinet/ip_icmp.h>   //Provides declarations for icmp header
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/tcp.h>   //Provides declarations for tcp header
#include<netinet/ip.h>    //Provides declarations for ip header

int total=0;
int size=0;
int StreamNumber = 0;
struct sockaddr_in source,dest;
#define NUMBER_OF_PACKETS 20	//Packets to capture

/*
 * Threads documentation and information consulted on : https://computing.llnl.gov/tutorials/pthreads/#Management
 */

pthread_t tid[NUMBER_OF_PACKETS];	//Maximum threads equal to the NUMBER_OF_PACKETS
pthread_cond_t condition[NUMBER_OF_PACKETS];	//Maximum conditions to wake up each thread
pthread_mutex_t mut[NUMBER_OF_PACKETS];			//Maximum mutex to lock conditions
pthread_rwlock_t blockPacket;

int packetsOut = 0;		//Packets out from the queue after the live capture
u_char packetToPrint[BUFSIZ];	//Global packet to print (shared by the threads!)

u_char packetArray[NUMBER_OF_PACKETS][BUFSIZ];
int front = 0;
int rear = -1;
int itemCount = 0;

/**
 * Validate the inserted interface name. Returns 1 on a valid interface and 0 on a non-valid interface
 * @param ifaceName contains the interface name
 */
int validateIface(char *ifaceName);

/*
 * Callback function called by pcap_loop to handle the received packets
 */

void process_packet(unsigned char *args, const struct pcap_pkthdr *header, const unsigned char *packet);

/*
 * Functions to manage the queue
 */

void insert(const u_char *buffer);
bool isFull();
bool isEmpty();
int sizeQueue();
u_char * removeData();

// ***************************************************************************************************************

/**
 * Function to verify if the stream already exists. It returns the thread id if exists
 * @param buffer contains the packet captured on the network
 */

int streamExists(const u_char *buffer);

/*
 * Functions to print the packet captured on libpcap
 */
void print_ethernet_header(const u_char *Buffer, int Size);
void print_ip_header(const u_char * Buffer, int Size);
void print_tcp_packet(const u_char * Buffer, int Size);

// ***************************************************************************************************************

/**
 * Callback function to all threads to print the packet to the specific file of the thread
 * @param StreamN Number of the stream (and thread) to print on the specific thread
 */

void *PrintFile(void *StreamN);

//Will consider bi-directional streams

struct IpPort{
	u_int16_t port;				//Port of ip packet
	struct sockaddr_in ipPack;	//Ip of packet
};

struct streamInfo{
	struct IpPort ipAndPortSrc;		//Src and Dst IP of the packet
	struct IpPort ipAndPortDst;		//Dst IP and port
};

struct streamInfo streamsStored[NUMBER_OF_PACKETS];	//Maximum number of streams possible (every packet different

#endif /* PACKETSNIFFER_H_ */
